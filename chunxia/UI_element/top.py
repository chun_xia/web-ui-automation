"""
首页所有元素
"""

from chunxia.UI_element.currency_method import FindElement


class TopElement(FindElement):

    @staticmethod
    def element_1():
        return TopElement.find('id', 'kw')

    @staticmethod
    def element_2():
        return TopElement.find('xpath', '//*[@id="suggestion"]')

    # element2 = FindElement('xpath', '//*[@id="kw"]')


if __name__ == '__main__':
    # 引用了定义类后，怎么在新类里面定义一组变量，让他能够别别处使
    TopElement.element_1().send_keys("查询")
    TopElement.element_2().send_keys("好耶")
    print('执行成功')