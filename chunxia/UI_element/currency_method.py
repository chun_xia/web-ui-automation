"""
coding
元素名称
定位类型
元素定位

输入框 = FindElement('id', 'kw').find()

所属模块
通过项目文件夹结构来管理，那么如何处理引用呢
"""
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By


# wd = webdriver.Chrome(service=Service(r'/Users/yangxiong/tools/chromedriver'))
wd = webdriver.Chrome()
wd.get('https://www.byhy.net/_files/stock1.html')


class FindElement(object):

    @staticmethod
    def find(method, location):
        if method == "id":
            return wd.find_element(By.ID, location)

        if method == "name":
            return wd.find_element(By.NAME, location)

        if method == "class":
            return wd.find_element(By.CLASS_NAME, location)

        if method == "tag":
            return wd.find_element(By.TAG_NAME, location)

        if method == "css":
            return wd.find_element(By.CSS_SELECTOR, location)

        if method == "link_txt":
            return wd.find_element(By.LINK_TEXT, location)

        if method == "plink":
            return wd.find_element(By.PARTIAL_LINK_TEXT, location)

        if method == "xpath":
            return wd.find_element(By.XPATH, location)

    # 类在初始化后，参数需要以self.来传递
    # def __init__(self, method, location):
    #     self.method = method
    #     self.location = location


if __name__ == '__main__':
    输入框 = FindElement().find("id", "kw")
    输入框.send_keys('通讯\n')
    print("hi,chunxia")