# WebUI自动化

#### 介绍
对常见Web网站进行UI自动化测试，主要使用Selenium实现

#### 使用技术栈&依赖
selenium客户端库
浏览器驱动（chrome driver、firefox driver等）
浏览器（chrome、firefox等）
python 注：笔者编写时使用版本为3.8.2



#### 分层思路&框架结构
层级1、元素库。封装常用的元素选择法，使元素定义只需要定位方式、具体定位表达式两个参数来确定。


#### 框架优点
1、针对业务系统页面元素经常变动的问题。引入元素库单独定义元素，编写CASE时只需引用元素。使UI自动化CASE与元素解耦，当元素变化时，修改元素即可。
也可以达成一个元素被多条CASE复用的效果。
2、常见场景CASE复用，无需每条CASE都写步骤
3、集成测试报告，直观展示测试结果


#### 改进点
1、提供脚手架，帮助使用者快速开始
2、尝试平台化，提供一个完整的SAAS式前后端分离的自动化平台，提供数据存储与多租户、权限的功能，可以让企业进行使用
3、把元素/浏览器配置写成config文件或者yaml文件来读取
4、提供运行时截图？感觉没必要，本地可以看见运行时浏览器的动作
5、提供用例运行时logging
6、CASE执行时间统计
7、引入pytest断言功能
8、allure测试报告


#### 整体原理
1、自动化程序调用Selenium 客户端库函数（比如点击按钮元素）
2、客户端库会发送Selenium 命令 给浏览器的驱动程序
3、浏览器驱动程序接收到命令后 ,驱动浏览器去执行命令
4、浏览器执行命令
5、浏览器驱动程序获取命令执行的结果，返回给我们自动化程序
6、自动化程序对返回结果进行处理


#### 使用示例
场景，登录并断言
##### 元素库


##### UI自动化场景

##### 测试报告


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

